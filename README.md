# Pokedex-Vue

Link: [live server](https://pokedexue.netlify.com/)

A pokemon library where you can search and view pokemon. Made with Vue and lots of love.

courtesy: pokeapi.co

## Project setup
```
1.Clone the repo
2.path/to/the/repo and npm install
```

### Compiles and hot-reloads for development
```
1.npm run serve 
2.Goto localhost:8080 in your browser


```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
