/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
var webpack = require("webpack");

module.exports = {
  chainWebpack: config => {
    config.module.rules.delete("eslint");
  }
};
